package game.apache.war.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckSubscribe {

    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("auto_charge")
    @Expose
    private String autoCharge;

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAutoCharge() {
        return autoCharge;
    }

    public void setAutoCharge(String autoCharge) {
        this.autoCharge = autoCharge;
    }

}
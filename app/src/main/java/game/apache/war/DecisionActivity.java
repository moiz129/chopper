package game.apache.war;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.billingclient.util.IabBroadcastReceiver;
import com.android.billingclient.util.Purchase;

import java.io.File;

import game.apache.war.Service.IAPHelper;

public class DecisionActivity extends BaseActivity {

//    private EditText phoneEditText;
    private ImageView firstSdkButton, secondSdkButton;
    private IAPHelper iapHelper;
    private IabBroadcastReceiver mBroadcastReceiver;
    private static final String TAG = "war";
    private static final int REQUEST_CODE_SUB = 10001;
    private static final String SKU = "apache_war";
    private boolean setup = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        firstSdkButton = findViewById(R.id.firstSdkButton);
        secondSdkButton = findViewById(R.id.secondSdkButton);

        firstSdkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DecisionActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        secondSdkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(DecisionActivity.this);
                dialogbuilder.setMessage("Are you sure you want to download the game agian ?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener);

                if (!checkIfApkExist()) {
                    charkhoneSetup("");
                } else {
                    dialogbuilder.show();
                }



            }
        });

    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    charkhoneSetup("");
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    finish();
                    break;
            }
        }
    };

    private void charkhoneSetup(String phone) {

        //"09030231579"
        iapHelper = IAPHelper.init(this, phone);

        if (setup) {
            iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
            return;
        }

        iapHelper.setup(this, new IAPHelper.SetupCallback() {
            @Override
            public void onError(String msg) {
                Log.e(TAG, "ERROR");
            }

            @Override
            public void onSuccess(IAPHelper helper) {
                showLoading(null, "لطفا صبر کنید...");
                setup = true;
                iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
            }

            @Override
            public void receivedBroadcast() {
                if (iapHelper == null) return;
                iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
                hideLoading();
            }
        });

    }


    private IAPHelper.QueryInventoryCallback queryInventoryCallback = new IAPHelper.QueryInventoryCallback() {
        @Override
        public void onError(String msg) {

            iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);

            setUnsubscribeState();
        }

        @Override
        public void onNotLogged() {
            if (iapHelper == null) return;
            startSubscription();
            setUnsubscribeState();
            hideLoading();

        }

        @Override
        public void onNoSCU() {
            if (iapHelper == null) return;
            startSubscription();
            setUnsubscribeState();
            hideLoading();
        }

        @Override
        public void onSCU(Purchase purchase) {
            if (purchase.isAutoRenewing()) {
                setSubscribeState();
                Intent intent = new Intent(DecisionActivity.this, WaitInstallActivity.class);
                startActivity(intent);
            } else {
                //startSubscription();
                setUnsubscribeState();
                //showSubscriptionInfo();
            }
            hideLoading();
        }
    };


    private void startSubscription() {
        if (iapHelper != null) {
            iapHelper.purchaseIAPSubscription(DecisionActivity.this, "apache_war", REQUEST_CODE_SUB, purchaseCallback);
        }
    }

    private IAPHelper.PurchaseCallback purchaseCallback = new IAPHelper.PurchaseCallback() {
        @Override
        public void onError(String msg) {
            if (iapHelper == null) return;

            //COMMENT THIS
            //iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
        }

        @Override
        public void onPurchased() {
            if (iapHelper == null) return;
            iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
        }

        @Override
        public void onNoPurchase() {
            Log.d(TAG, "onNoPurchase");
//            if (iapHelper == null) return;
            //iapHelper.purchaseIAPSubscription(MainActivity.this, getSubscriptionSCU(), REQUEST_CODE_SUB, purchaseCallback);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!iapHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
        try {
            //requestWindowFeature(Window.FEATURE_NO_TITLE);
            getActionBar().hide();
        } catch (Exception e) {
            Log.d(TAG, "onActivityResult");
        }
    }

    private void setUnsubscribeState() {
        //hideMenu();
        deleteSubscriptionFile();
    }

    private void setSubscribeState() {
//        showMenu();
        createSubscriptionFile();
    }

    private void createSubscriptionFile() {
        File file = new File(getApplicationContext().getFilesDir(), "subscription_info_agree.txt");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private boolean isSubscribeInfoAgree() {
        return new File(getApplicationContext().getFilesDir(), "subscription_info_agree.txt").exists();
    }

    private void deleteSubscriptionFile() {
        File file = new File(getApplicationContext().getFilesDir(), "subscription_info_agree.txt");
        if (file.exists()) {
            file.delete();
        }
    }

    private boolean checkIfApkExist() {

        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        final String fileName = Constants.FILE_NAME;
        destination += fileName;
        File file = new File(destination);

        if (file.exists()) {
            return true;
        }

        return false;

    }
}

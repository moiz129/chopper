package game.apache.war;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.billingclient.util.IabBroadcastReceiver;
import com.android.billingclient.util.Purchase;

import java.io.File;

import game.apache.war.Model.CheckSubscribe;
import game.apache.war.Service.IAPHelper;
import game.apache.war.Service.SharedPrefService;
import game.apache.war.Service.SubscriptionService;
import game.apache.war.api.ApiClient;
import game.apache.war.api.SubscriptionApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private EditText phoneEditText;
    private Button sendBtn;
    private IAPHelper iapHelper;
    private IabBroadcastReceiver mBroadcastReceiver;
    private static final String TAG = "war";
    private static final int REQUEST_CODE_SUB = 10001;
    private static final String SKU = "apache_war";
    private boolean setup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        phoneEditText = findViewById(R.id.phoneNumber);
        sendBtn = findViewById(R.id.sendBtn);

        getSupportActionBar().hide();

        AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
        dialogbuilder.setMessage("Are you sure you want to download the game agian ?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener);


        if (!checkIfApkExist()) {
            checkSubscription();
        } else {
            dialogbuilder.show();
        }

        //09030231579


        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateNumber(phoneEditText.getText().toString())) {
                    subscribe();
                    if (phoneEditText.getText().toString().substring(0, 1).equals("0")) {
                        String number = phoneEditText.getText().toString().substring(1);
                        SharedPrefService.getInstance(MainActivity.this).savePhone("98" + number);
                    }
                } else {
                    Toast.makeText(MainActivity.this, SubscriptionService.getErrorMessage("1005"), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void checkSubscription() {

        String phone = SharedPrefService.getInstance(this).getPhone();

        if (!phone.isEmpty()) {
            if (phone.substring(0, 2).equals("98")) {
                String number = phone.substring(2, phone.length() - 1);
                phoneEditText.setHint("0" + number);

            } else
                phoneEditText.setHint(phone);

            final SubscriptionApi subscriptionApi = ApiClient.getClient().create(SubscriptionApi.class);
            Call<CheckSubscribe> call = subscriptionApi.checkSubscription(phone, Constants.SUBSCRIBE, Constants.SID);
            showLoading(null, "لطفا صبر کنید...");
            call.enqueue(new Callback<CheckSubscribe>() {
                @Override
                public void onResponse(Call<CheckSubscribe> call, Response<CheckSubscribe> response) {

                    hideLoading();

                    if (response.body().getStatus().equals("-1") ||
                            response.body().getStatus().equals("unsub")) {
                        subscribe();

                    } else if (response.body().getStatus().equals("sub") ||
                            response.body().getStatus().equals("SUBSCRIPTION") ||
                            response.body().getStatus().equals("AUTO_CHARGE") ||
                            response.body().getStatus().equals("UNSUBSCRIPTION")) {

                        Toast.makeText(MainActivity.this, SubscriptionService.getErrorMessage("0"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
//                        charkhoneSetup("");
                        Toast.makeText(MainActivity.this, SubscriptionService.getErrorMessage("-9999"), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckSubscribe> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                    hideLoading();
//                    charkhoneSetup("");
                }
            });


        } else {
            // Not Subscribe
        }

    }

    private void subscribe() {

        final SubscriptionApi subscriptionApi = ApiClient.getClient().create(SubscriptionApi.class);
        Call<String> call = subscriptionApi.subscribe(phoneEditText.getText().toString(), Constants.SUBSCRIBE, Constants.SID);
        showLoading(null, "لطفا صبر کنید...");

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideLoading();

                String error = SubscriptionService.getErrorMessage(response.body());
                Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();

                if (response.body().equals("0")) {

                    Intent i = new Intent(MainActivity.this, CodeActivity.class);
                    i.putExtra("phone", phoneEditText.getText().toString());
                    startActivity(i);
                    finish();
                } else {
                    //failed
//                    charkhoneSetup(phoneEditText.getText().toString());

                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                hideLoading();
//                charkhoneSetup(phoneEditText.getText().toString());
            }
        });
    }

    private boolean validateNumber(String phone) {
        if (phone.isEmpty()) {
            return false;
        }

        if (phone.length() < 11) {
            return false;
        }

        return true;
    }


//    private void charkhoneSetup(String phone) {
//
//        //"09030231579"
//        iapHelper = IAPHelper.init(this, phone);
//
//        if (setup) {
//            iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
//            return;
//        }
//
//        iapHelper.setup(this, new IAPHelper.SetupCallback() {
//            @Override
//            public void onError(String msg) {
//                Log.e(TAG, "ERROR");
//            }
//
//            @Override
//            public void onSuccess(IAPHelper helper) {
//                showLoading(null, "لطفا صبر کنید...");
//                setup = true;
//                iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
//            }
//
//            @Override
//            public void receivedBroadcast() {
//                if (iapHelper == null) return;
//                iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
//                hideLoading();
//            }
//        });
//
//    }
//
//
//    private IAPHelper.QueryInventoryCallback queryInventoryCallback = new IAPHelper.QueryInventoryCallback() {
//        @Override
//        public void onError(String msg) {
//
//            iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
//
//            setUnsubscribeState();
//        }
//
//        @Override
//        public void onNotLogged() {
//            if (iapHelper == null) return;
//            startSubscription();
//            setUnsubscribeState();
//            hideLoading();
//
//        }
//
//        @Override
//        public void onNoSCU() {
//            if (iapHelper == null) return;
//            startSubscription();
//            setUnsubscribeState();
//            hideLoading();
//        }
//
//        @Override
//        public void onSCU(Purchase purchase) {
//            if (purchase.isAutoRenewing()) {
//                setSubscribeState();
//                Intent intent = new Intent(MainActivity.this, WaitInstallActivity.class);
//                startActivity(intent);
//            } else {
//                //startSubscription();
//                setUnsubscribeState();
//                //showSubscriptionInfo();
//            }
//            hideLoading();
//        }
//    };
//
//
//    private void startSubscription() {
//        if (iapHelper != null) {
//            iapHelper.purchaseIAPSubscription(MainActivity.this, "apache_war", REQUEST_CODE_SUB, purchaseCallback);
//        }
//    }
//
//    private IAPHelper.PurchaseCallback purchaseCallback = new IAPHelper.PurchaseCallback() {
//        @Override
//        public void onError(String msg) {
//            if (iapHelper == null) return;
//
//            //COMMENT THIS
//            //iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
//        }
//
//        @Override
//        public void onPurchased() {
//            if (iapHelper == null) return;
//            iapHelper.queryInventoryForSCU(SKU, queryInventoryCallback);
//        }
//
//        @Override
//        public void onNoPurchase() {
//            Log.d(TAG, "onNoPurchase");
////            if (iapHelper == null) return;
//            //iapHelper.purchaseIAPSubscription(MainActivity.this, getSubscriptionSCU(), REQUEST_CODE_SUB, purchaseCallback);
//        }
//    };
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (!iapHelper.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data);
//        } else {
//            Log.d(TAG, "onActivityResult handled by IABUtil.");
//        }
//        try {
//            //requestWindowFeature(Window.FEATURE_NO_TITLE);
//            getActionBar().hide();
//        } catch (Exception e) {
//            Log.d(TAG, "onActivityResult");
//        }
//    }
//
//    private void setUnsubscribeState() {
//        //hideMenu();
//        deleteSubscriptionFile();
//    }
//
//    private void setSubscribeState() {
////        showMenu();
//        createSubscriptionFile();
//    }
//
//    private void createSubscriptionFile() {
//        File file = new File(getApplicationContext().getFilesDir(), "subscription_info_agree.txt");
//        if (!file.exists()) {
//            file.mkdir();
//        }
//    }
//
//    private boolean isSubscribeInfoAgree() {
//        return new File(getApplicationContext().getFilesDir(), "subscription_info_agree.txt").exists();
//    }
//
//    private void deleteSubscriptionFile() {
//        File file = new File(getApplicationContext().getFilesDir(), "subscription_info_agree.txt");
//        if (file.exists()) {
//            file.delete();
//        }
//    }


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    checkSubscription();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    finish();
                    break;
            }
        }
    };

    private boolean checkIfApkExist() {

        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        final String fileName = Constants.FILE_NAME;
        destination += fileName;
        File file = new File(destination);

        if (file.exists()) {
            return true;
        }

        return false;

    }

}

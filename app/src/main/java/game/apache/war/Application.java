package game.apache.war;

import net.jhoobin.jhub.CharkhoneSdkApp;

/**
 * Created by muhammadmoiz on 2/3/19.
 */

public class Application extends CharkhoneSdkApp {

    @Override
    public String[] getSecrets() {
        return getResources().getStringArray(R.array.secrets);
    }
}

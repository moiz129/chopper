package game.apache.war;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

import game.apache.war.Service.SubscriptionService;
import game.apache.war.api.ApiClient;
import game.apache.war.api.SubscriptionApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CodeActivity extends BaseActivity {

    private Button verifyBtn;
    private EditText verificationCode;
    private final int PERMISSION_REQUEST_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_code);

        verifyBtn = findViewById(R.id.verifyBtn);
        verificationCode = findViewById(R.id.verificationCode);

        getSupportActionBar().hide();

        Intent intent = getIntent();
        final String phone = intent.getStringExtra("phone");

        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyToken(phone);
            }
        });
    }

    private void verifyToken(final String phone) {
        final SubscriptionApi subscriptionApi = ApiClient.getClient().create(SubscriptionApi.class);

//        if (Build.VERSION.SDK_INT >= 23) {
//            if (checkPermission()) {
//                installApplication();
//
//            } else {
//                requestPermission(); // Code for permission
//            }
//        } else {
//            installApplication();
//        }

        if (!verificationCode.getText().toString().isEmpty()) {

            Call<String> call = subscriptionApi.verifySubscriptionToken(phone, Constants.SUBSCRIBE, Constants.SID, verificationCode.getText().toString());

            showLoading(null, "لطفا صبر کنید...");

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    hideLoading();
                    String error = SubscriptionService.getErrorMessage(response.body());

                    if (response.body().equals("0")) {
//                        if (Build.VERSION.SDK_INT >= 23) {
//                            if (checkPermission()) {
//                                installApplication();
//
//                            } else {
//                                requestPermission(); // Code for permission
//                            }
//                        } else {
//                            installApplication();
//                        }

                        goToWaitInstallActivity();
                    }

                    Toast.makeText(CodeActivity.this, error, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(CodeActivity.this, "خطا", Toast.LENGTH_SHORT).show();
                    hideLoading();
                }
            });

        }

    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(CodeActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(CodeActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

        } else {
            ActivityCompat.requestPermissions(CodeActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    installApplication();
                } else {

                }
                break;
        }
    }


    /**
     * Call this method to download and install application
     */
    public void installApplication() {

        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
//        final String fileName = "Copy of جنگ با هلیکوپتر.apk";
//        final String fileName = "Copy of جنگ با هلیکوپتر.apk";
        final String fileName = "chopper.apk";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        //Delete update file if exists
        File file = new File(destination);
        if (file.exists())
            //file.delete() - test this, I think sometimes it doesnt work
            file.delete();

        //get url of app on server
        String url = Constants.APP_DOWNLOAND_URL;

        //set downloadmanager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Downloading APK");
        request.setTitle("Downloading.");

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {

                File toInstall = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri apkUri = FileProvider.getUriForFile(CodeActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", toInstall);
                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    install.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    install.setDataAndType(apkUri, "application/vnd.android.package-archive");
                    startActivity(install);
//                    Intent install = new Intent(Intent.ACTION_VIEW);
//                    install.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    install.setDataAndType(apkUri, "application/vnd.android.package-archive");
//                    startActivity(install);

                } else {
                    Uri apkUri = Uri.fromFile(toInstall);
                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setDataAndType(apkUri, "application/vnd.android.package-archive");
                    install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(install);
                }

                unregisterReceiver(this);
                finish();

            }
        };
        //register receiver for when .apk download is compete
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void goToWaitInstallActivity() {
        Intent intent = new Intent(CodeActivity.this, WaitInstallActivity.class);
        startActivity(intent);
        finish();
    }


}

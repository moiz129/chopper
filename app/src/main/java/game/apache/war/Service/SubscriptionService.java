package game.apache.war.Service;

import java.util.HashMap;
import java.util.Map;

import game.apache.war.Constants;

/**
 * Created by muhammadmoiz on 2/2/19.
 */

public class SubscriptionService {

    private static Map<String, String> errors = new HashMap<>();

    static {
        errors.put("0", "عملیات موفق");
        errors.put("-11", "اطلاعات یافت نشد");
        errors.put("-12", "اطلاعات یافت نشد");
        errors.put("-13", "زمان تمام شد");
        errors.put("-14", "خطای سیستم");
        errors.put("-15", "خطای سیستم");
        errors.put("-101", "سرویس قابل دسترس نیست");
        errors.put("-102", "خطای سیستم");
        errors.put("-103", "شماره وارد شده غیر فعال است");
        errors.put("-104", "خطای سیستم");
        errors.put("-105", "خطای سیستم"); //RUN_NOT_EXIST
        errors.put("-106", "خطای سیستم");  //EVENT_NOT_EXIST
        errors.put("-107", "خطای سیستم");  //EPMTY_CONTENT
        errors.put("-107", "خطای سیستم");
        errors.put("-9998", "خطای سیستم"); //NOT_IMPLEMENTED
        errors.put("-9999", "خطای سیستم");

        errors.put("1001", "کلمه عبور یا رمز اشتباه است");
        errors.put("1002", "شماره تلفن همراه اشتباه است");
        errors.put("1003", "کد سرویس اشتباه است");

        errors.put("1005", "شماره تلفن همراه اشتباه است");
        errors.put("1006", "شناسه سرویس اشتباه است");

        errors.put("1008", "خطای سیستم");
        errors.put("1009", "خطای سیستم");

        errors.put("1012", "خطای سیستم");  //READ_TIMEOUT
        errors.put("1013", "خطای سیستم");    //CONNECTION_TIMEOUT

        errors.put("1015", "کد ورودی اشتباه است");
        errors.put("1017", "ائید کد با موفقیت انجام شد");

        errors.put("5030", "خطای سیستم");  //UNKNOWN_OCS_ERROR
        errors.put("5353", "خطای سیستم");

    }

    public static String getErrorMessage(String key) {
        return errors.get(key);
    }

    public static boolean processResponse(String body) {

        if (body.equals(Constants.SUCCESS)) {
            return true;
        }

        return false;

    }

}

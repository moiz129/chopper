package game.apache.war.Service;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by muhammadmoiz on 2/5/19.
 */

public class SharedPrefService {

    private static String SHARED_PREF_KEY = "game.combat.chopper";
    private static SharedPrefService instance;
    private SharedPreferences privateSharedPreferences;

    private final String PHONE = "phone";

    private SharedPrefService(Context context) {
        this.privateSharedPreferences = context.getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
    }


    public static SharedPrefService getInstance(Context context) {

        if(instance == null) {
            synchronized (SharedPrefService.class) {
                if (instance == null) {
                    instance = new SharedPrefService(context);
                }
                return instance;
            }
        }
        return instance;
    }

    public void savePhone(String phone) {
        SharedPreferences.Editor editor = privateSharedPreferences.edit();
        editor.putString(PHONE, phone);
        editor.apply();
    }

    public String getPhone() {
        return privateSharedPreferences.getString(PHONE, "");
    }

}

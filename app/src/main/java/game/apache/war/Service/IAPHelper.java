package game.apache.war.Service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.android.billingclient.util.IabBroadcastReceiver;
import com.android.billingclient.util.IabHelper;
import com.android.billingclient.util.IabResult;
import com.android.billingclient.util.Inventory;
import com.android.billingclient.util.Purchase;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class IAPHelper {

    private static final String IAP_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDZVek+9Ue70RwPpH/jBfGNLYjXFGBMTQGM57ktRz9nKbpVvX2j2hDSIMK7vxH1FVjjN1llVGd/4xmsJUck43aSqj+Cyrjv6PHun18PHLeTmc9VyoHfRzATVS8Sl88SQDfVAI0g8WP0yLDQbE5ALJrbvZLeh0gyHvflOMgbxT9EBQIDAQAB";
    private static final int IAP_ERROR_NOT_LOGGED = 6;
    private static final String APP_PACKAGE = "timer.fity.me.beginner";
    private static final String IAP_ACCESS_TOKEN = "b5f001e3-1c9d-3e11-b139-20120900410f";
    private static IAPHelper iapHelper;
    private IabHelper iabHelper;
    private IabBroadcastReceiver mBroadcastReceiver;
    private Purchase subscription;


    public interface SetupCallback extends IabBroadcastReceiver.IabBroadcastListener{
        void onError(String msg);
        void onSuccess(IAPHelper helper);
        void receivedBroadcast();
    }

    public interface QueryInventoryCallback {
        void onError(String msg);
        void onNotLogged();
        void onNoSCU();
        void onSCU(Purchase purchase);
    }

    public interface PurchaseCallback {
        void onError(String msg);
        void onPurchased();
        void onNoPurchase();
    }

    public interface UnSubscribeCallback {
        void onError(String msg);
        void onUnSubscribe();
    }

    private IAPHelper(Context context, String phoneNumber) {
        iabHelper = new IabHelper(context, IAP_PUBLIC_KEY);
        //iabHelper.enableDebugLogging(true, "IAPHelper");
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            Intent fillIntent = new Intent();
            fillIntent.putExtra("msisdn", phoneNumber);
            fillIntent.putExtra("editable", true);
            iabHelper.setFillInIntent(fillIntent);
        }
    }

    public static IAPHelper init(Context context) {
        return init(context, "");
    }

    public static IAPHelper init(Context context, String phoneNumber) {
        if (iapHelper == null) {
            iapHelper = new IAPHelper(context, phoneNumber);
        }
        return iapHelper;
    }

    public void setup(final Activity activity, final SetupCallback callback) {
        if (iabHelper == null) return;
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    //Log.e("TAG", "start up error");
                    callback.onError(result.getMessage());
                } else {
                    //Log.e("TAG", "start up success");
                    mBroadcastReceiver = new IabBroadcastReceiver(callback);
                    IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                    activity.registerReceiver(mBroadcastReceiver, broadcastFilter);
                    callback.onSuccess(IAPHelper.this);
                }
            }
        });
    }

    private void setSubscription(Purchase purchase) {
        subscription = purchase;
    }

    public void dispose(Activity activity) {
        if (mBroadcastReceiver != null) {
            activity.unregisterReceiver(mBroadcastReceiver);
        }
        if (iabHelper != null) {
            iabHelper.disposeWhenFinished();
            iabHelper = null;
        }
        iapHelper = null;
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        if(iabHelper == null) return false;
        return iabHelper.handleActivityResult(requestCode, resultCode, data);
    }

    public void queryInventoryForSCU(final String SCU, final QueryInventoryCallback callback) {
        if (iabHelper == null) return;
        try {
            iabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
                @Override
                public void onQueryInventoryFinished(IabResult iabResult, Inventory inventory) {
                    if (iabResult.isFailure()) {
                        if (iabResult.getResponse() == IAP_ERROR_NOT_LOGGED) {
                            callback.onNotLogged();
                        } else {
                            callback.onError(iabResult.getMessage());
                        }
                    } else {
                        Purchase purchase = inventory.getPurchase(SCU);
                        if (purchase == null) {
                            callback.onNoSCU();
                        } else {
                            setSubscription(purchase);
                            callback.onSCU(purchase);
                        }
                    }



                }
            });
        } catch (IabHelper.IabAsyncInProgressException e) {
            callback.onError("Error querying inventory. Another async operation in progress.");
        }
    }

    public void purchaseIAPSubscription(Activity activity, final String SCU, int requestCode, final PurchaseCallback callback) {
        if (iabHelper == null) return;
        try {
            iabHelper.launchSubscriptionPurchaseFlow(activity, SCU, requestCode, new IabHelper.OnIabPurchaseFinishedListener() {
                @Override
                public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
                    if (iabResult.isFailure()) {
                        callback.onError(iabResult.getMessage());
                    }
                    if (iabHelper == null) return;
                    if (purchase != null && purchase.getSku().equals(SCU)) {
                        setSubscription(purchase);
                        callback.onPurchased();
                    } else {
                        callback.onNoPurchase();
                    }
                }
            }, "");
        } catch (IabHelper.IabAsyncInProgressException e) {
           callback.onError("Error querying inventory. Another async operation in progress.");
        }
    }

    public void unsubscribe(final Activity activity, String SCU, final UnSubscribeCallback callback) {
        if (subscription != null) {
            OkHttpClient client = new OkHttpClient();
            final Request request = new Request.Builder()
                    .url("https://seller.jhoobin.com/ws/androidpublisher/v2/applications/" + APP_PACKAGE + "/purchases/subscriptions/" + SCU + "/tokens/" + subscription.getToken() + ":cancel?access_token=" + IAP_ACCESS_TOKEN)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(e.getMessage());
                        }
                    });
                }

                @Override
                public void onResponse(final Call call, Response response) throws IOException {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onUnSubscribe();
                        }
                    });
                }
            });
        }

    }

//    public static boolean isNetworkAvailable(Context context) {
//        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        return cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
//    }
}
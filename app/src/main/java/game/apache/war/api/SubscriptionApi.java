package game.apache.war.api;

import game.apache.war.Constants;

import game.apache.war.Model.CheckSubscribe;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by muhammadmoiz on 1/31/19.
 */

public interface SubscriptionApi {

    @GET(Constants.SUBSCRIBE_API)
    Call<String> subscribe(@Query("Msisdn") String msisdn, @Query("Action") String action, @Query("Sid") String sid);


    @POST(Constants.SUBSCRIBE_API)
    Call<String> verifySubscriptionToken(@Query("Msisdn") String msisdn, @Query("Action") String action, @Query("Sid") String sid, @Query("Token") String token);

    @GET(Constants.CHECK_SUBSCRIBE_API)
    Call<CheckSubscribe> checkSubscription(@Query("Msisdn") String msisdn, @Query("Action") String action, @Query("Sid") String sid);

}
